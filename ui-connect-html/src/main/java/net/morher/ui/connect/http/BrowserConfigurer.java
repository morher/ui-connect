package net.morher.ui.connect.http;

import com.gargoylesoftware.htmlunit.WebClient;

public interface BrowserConfigurer {
    void configure(WebClient webClient);
}