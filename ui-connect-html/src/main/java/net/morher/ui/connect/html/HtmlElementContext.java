package net.morher.ui.connect.html;

import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlTextArea;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import net.morher.ui.connect.api.action.Choosable;
import net.morher.ui.connect.api.action.Clickable;
import net.morher.ui.connect.api.action.ReadableInput;
import net.morher.ui.connect.api.action.ReadableLabel;
import net.morher.ui.connect.api.action.Toggleable;
import net.morher.ui.connect.api.action.WriteableInput;

public class HtmlElementContext implements Clickable, ReadableLabel, ReadableInput, WriteableInput, Toggleable, Choosable {
    private final HtmlElementContext parent;
    private HtmlElement element;

    public HtmlElementContext(HtmlElementContext parent, HtmlElement element) {
        this.parent = parent;
        this.element = element;
    }

    public HtmlElement getElement() {
        return element;
    }

    @SuppressWarnings("unchecked")
    public <E extends HtmlElement> E getElementAs(Class<E> elementType) {
        HtmlElement element = getElement();
        if (elementType.isInstance(element)) {
            return (E) element;
        }
        throw new RuntimeException("Element er av uventet type");
    }

    public HtmlElementContext getParent() {
        return parent;
    }

    @Override
    public String getText() {
        return getElementAs(HtmlElement.class).asText();
    }

    @Override
    public String getValue() {
        if (element instanceof HtmlTextArea) {
            return ((HtmlTextArea) element).getText();
        }
        return getElementAs(HtmlInput.class).getValueAttribute();
    }

    @Override
    public void setValue(String value) {
        if (element instanceof HtmlTextArea) {
            ((HtmlTextArea) element).setText(value);
        } else {
            getElementAs(HtmlInput.class).setValueAttribute(value);
        }
    }

    @Override
    public void click() {
        try {
            getElement().click();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void doubleClick() {
        try {
            getElement().dblClick();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isEnabled() {
        if (element instanceof HtmlInput) {
            return ((HtmlInput) element).isChecked();
        }
        return false;
    }

    @Override
    public void toggle(boolean enabled) {
        if (element instanceof HtmlInput) {
            ((HtmlInput) element).setChecked(enabled);
        }
    }

    @Override
    public void toggle() {
        toggle(!isEnabled());
    }

    @Override
    public void select(String option) {
        if (element instanceof HtmlSelect) {
            for (HtmlOption htmlOption : ((HtmlSelect) element).getOptions()) {
                if (Objects.equals(option, htmlOption.asText())
                        || Objects.equals(option, htmlOption.getValueAttribute())) {
                    htmlOption.setSelected(true);
                }
            }
        }
    }

    @Override
    public List<String> getOptions() {
        List<String> options = new ArrayList<>();
        if (element instanceof HtmlSelect) {
            for (HtmlOption htmlOption : ((HtmlSelect) element).getOptions()) {
                options.add(htmlOption.asText());
            }
        }
        return options;
    }
}
