package net.morher.ui.connect.html;

import java.util.Collection;

import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;

import net.morher.ui.connect.api.annotation.Id;
import net.morher.ui.connect.api.annotation.Name;
import net.morher.ui.connect.api.element.View;
import net.morher.ui.connect.api.mapping.ActionHandlerFactory;
import net.morher.ui.connect.api.mapping.ContextPassThroughActionHandlerFactory;
import net.morher.ui.connect.api.mapping.ElementLocater;
import net.morher.ui.connect.api.mapping.LocatorDescription;
import net.morher.ui.connect.api.mapping.PassThroughLocator;
import net.morher.ui.connect.api.mapping.UserInterfaceMapper;

public class HtmlMapper implements UserInterfaceMapper<HtmlElementContext> {
    private static final ContextPassThroughActionHandlerFactory<HtmlElementContext> CONTEXT_PASS_THROUGH_ACTION_HANDLER_FACTORY = new ContextPassThroughActionHandlerFactory<>();

    @Override
    public ElementLocater<HtmlElementContext> buildLocator(LocatorDescription desc) {
        CssSelector cssSelector = desc.findAnnotation(CssSelector.class);
        if (cssSelector != null) {
            return new HtmlSelectorLocator(cssSelector.value());
        }
        Id elementId = desc.findAnnotation(Id.class);
        if (elementId != null) {
            return new HtmlSelectorLocator("#" + elementId.value());
        }
        Name elementName = desc.findAnnotation(Name.class);
        if (elementName != null) {
            return new HtmlSelectorLocator("*[name='" + elementName.value() + "']");
        }
        if (desc.targetIsA(View.class)) {
            return new PassThroughLocator<>();
        }
        return null;
    }

    @Override
    public ActionHandlerFactory<HtmlElementContext, ?> getActionHandlerFactory() {
        return CONTEXT_PASS_THROUGH_ACTION_HANDLER_FACTORY;
    }

    public static class HtmlSelectorLocator implements ElementLocater<HtmlElementContext> {
        private final String selectors;

        public HtmlSelectorLocator(String selectors) {
            this.selectors = selectors;
        }

        @Override
        public Collection<HtmlElementContext> locate(HtmlElementContext parentContext) {
            return FluentIterable
                    .from(parentContext.getElement().querySelectorAll(selectors))
                    .filter(HtmlElement.class)
                    .transform(new ElementToContextConverter(parentContext))
                    .toList();
        }
    }

    public static class ElementToContextConverter implements Function<HtmlElement, HtmlElementContext> {
        private final HtmlElementContext parentContext;

        public ElementToContextConverter(HtmlElementContext parentContext) {
            this.parentContext = parentContext;
        }

        @Override
        public HtmlElementContext apply(HtmlElement element) {
            return new HtmlElementContext(parentContext, element);
        }
    }

}
