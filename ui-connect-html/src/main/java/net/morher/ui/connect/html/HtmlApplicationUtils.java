package net.morher.ui.connect.html;

import com.gargoylesoftware.htmlunit.html.HtmlElement;
import net.morher.ui.connect.api.ApplicationUtils;
import net.morher.ui.connect.api.element.Element;

public class HtmlApplicationUtils {

    public static HtmlElement getHtmlElement(Element element) {
        Object htmlElement = ApplicationUtils.getElementContext(element).getElementLink();
        if (htmlElement instanceof HtmlElementContext) {
            return ((HtmlElementContext) htmlElement).getElement();
        }
        throw new IllegalArgumentException("Not an HtmlElement");
    }

    public static String getHtml(Element element) {
        HtmlElement htmlElement = getHtmlElement(element);
        return htmlElement.asXml();
    }
}
