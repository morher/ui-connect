package net.morher.ui.connect.html.listener;

import net.morher.ui.connect.api.handlers.ElementMethodInvocation;
import net.morher.ui.connect.api.handlers.MethodHandler;
import net.morher.ui.connect.api.listener.BaseElementListener;
import net.morher.ui.connect.html.HtmlElementContext;

public class WaitForJavaScriptListener extends BaseElementListener<HtmlElementContext> {
    private static final int DEFAULT_WAIT_TIME = 30000;
    private final int waitBefore;
    private final int waitAfter;

    public WaitForJavaScriptListener() {
        this(DEFAULT_WAIT_TIME, DEFAULT_WAIT_TIME);
    }

    public WaitForJavaScriptListener(int waitBefore, int waitAfter) {
        this.waitBefore = waitBefore;
        this.waitAfter = waitAfter;
    }

    @Override
    public void beforeInvocation(ElementMethodInvocation<?, ? extends HtmlElementContext> invocation, MethodHandler<? extends HtmlElementContext> handler) {
        waitForBackgroundJavaScript(invocation, waitBefore);
    }

    @Override
    public void afterInvocation(ElementMethodInvocation<?, ? extends HtmlElementContext> invocation, MethodHandler<? extends HtmlElementContext> handler, Object returnValue) {
        waitForBackgroundJavaScript(invocation, waitAfter);
    }

    private void waitForBackgroundJavaScript(ElementMethodInvocation<?, ? extends HtmlElementContext> invocation, int waitTime) {
        HtmlElementContext elementLink = invocation.getElementContext().getElementLink();
        elementLink.getElement().getPage().getWebClient().waitForBackgroundJavaScript(waitTime);
    }

}
