package net.morher.ui.connect.api.element;

import net.morher.ui.connect.api.action.ElementAction;
import net.morher.ui.connect.api.action.ReadableLabel;

/**
 * An {@link Element} stereotype representing a label shown in the user interface. It extends the {@link ElementAction}
 * {@link ReadableLabel}.
 * 
 * @author Morten Hermansen
 */
public interface Label extends Element, ReadableLabel {

}
