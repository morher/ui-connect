package net.morher.ui.connect.api.strategy;

import java.lang.reflect.Method;
import net.morher.ui.connect.api.element.Element;

/**
 * A factory creating a {@link MethodStrategy} for a compatible {@link Element}-method.
 * 
 * @author Morten Hermansen
 * 
 * @see MethodStrategy
 */
public interface MethodStrategyFactory {

    /**
     * Considers if the given {@link Element}-method is compatible with the {@link MethodStrategyFactory} and potentially
     * creates a {@link MethodStrategy}.
     * 
     * @param method
     *            The {@link Element}-method to consider.
     * @return A {@link MethodStrategy} if the method is compatible, otherwise null.
     */
    MethodStrategy getMethodStrategy(Method method);
}
