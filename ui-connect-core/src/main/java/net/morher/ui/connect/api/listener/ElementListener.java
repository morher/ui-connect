package net.morher.ui.connect.api.listener;

import net.morher.ui.connect.api.handlers.ElementMethodInvocation;
import net.morher.ui.connect.api.handlers.MethodHandler;

public interface ElementListener<L> {
    void beforeInvocation(ElementMethodInvocation<?, ? extends L> invocation, MethodHandler<? extends L> handler);

    void afterInvocation(ElementMethodInvocation<?, ? extends L> invocation, MethodHandler<? extends L> handler, Object returnValue);

    void invocationException(ElementMethodInvocation<?, ? extends L> invocation, MethodHandler<? extends L> handler, Exception e);
}
