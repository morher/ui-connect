package net.morher.ui.connect.api;

import java.util.ArrayList;
import java.util.List;

import net.morher.ui.connect.api.strategy.CallImplementationStrategyFactory;
import net.morher.ui.connect.api.strategy.ElementPassThroughStrategyFactory;
import net.morher.ui.connect.api.strategy.LocateElementStrategyFactory;
import net.morher.ui.connect.api.strategy.MethodStrategyFactory;
import net.morher.ui.connect.api.strategy.PerformActionStrategyFactory;

public class ApplicationParserConfiguration {
    private static final ApplicationParserConfiguration defaultConfigurator = new ApplicationParserConfiguration();
    private List<MethodStrategyFactory> methodStrategyFactories = new ArrayList<>();

    public static ApplicationParserConfiguration getDefault() {
        return defaultConfigurator;
    }

    public ApplicationParserConfiguration() {
        methodStrategyFactories.add(new CallImplementationStrategyFactory());
        methodStrategyFactories.add(new ElementPassThroughStrategyFactory(AutoCloseable.class));
        methodStrategyFactories.add(new PerformActionStrategyFactory());
        methodStrategyFactories.add(new LocateElementStrategyFactory());
    }

    public List<MethodStrategyFactory> getMethodStrategyFactories() {
        return methodStrategyFactories;
    }

}
