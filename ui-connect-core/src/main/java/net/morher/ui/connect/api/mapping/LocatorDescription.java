package net.morher.ui.connect.api.mapping;

import java.lang.annotation.Annotation;

public interface LocatorDescription {

    <A extends Annotation> A findAnnotation(Class<A> annotationType);

    @Deprecated
    Class<?> getTargetClass();

    boolean targetIsA(Class<?> clazz);
}
