package net.morher.ui.connect.api.action;

public interface ReadableInput extends ElementAction {
    String getValue();
}
