package net.morher.ui.connect.api.handlers;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.morher.ui.connect.api.element.Element;

public class ElementHandlerRegistry<L> {
    private final Map<Class<?>, ElementHandler<?, L>> handlers = new HashMap<>();

    public <E extends Element> void register(ElementHandler<E, L> handler) {
        handlers.put(handler.getElementType(), handler);
    }

    @SuppressWarnings("unchecked")
    public <E extends Element> ElementHandler<E, L> get(Class<E> elementType) {
        return (ElementHandler<E, L>) handlers.get(elementType);
    }

    public Collection<ElementHandler<?, L>> getAll() {
        return handlers.values();
    }
}
