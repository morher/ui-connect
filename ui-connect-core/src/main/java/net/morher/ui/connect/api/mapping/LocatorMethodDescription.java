package net.morher.ui.connect.api.mapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class LocatorMethodDescription implements LocatorDescription {
    private final Method method;
    private final Class<?> targetClass;

    public LocatorMethodDescription(Method method, Class<?> targetClass) {
        this.method = method;
        this.targetClass = targetClass;
    }

    @Override
    public <A extends Annotation> A findAnnotation(Class<A> annotationType) {
        A annotation = method.getAnnotation(annotationType);
        if (annotation == null) {
            annotation = targetClass.getAnnotation(annotationType);
        }
        return annotation;
    }

    @Override
    public Class<?> getTargetClass() {
        return targetClass;
    }

    @Override
    public boolean targetIsA(Class<?> clazz) {
        return clazz.isAssignableFrom(targetClass);
    }

}
