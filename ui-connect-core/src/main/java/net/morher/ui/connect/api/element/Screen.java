package net.morher.ui.connect.api.element;

/**
 * <p>
 * An {@link Element} stereotype representing a screen, typically a page or window.
 * 
 * <p>
 * <strong>Example:</strong> <blockquote>
 * 
 * <pre>
 * public interface LoginScreen extends Screen {
 *     TextField username();
 * 
 *     TextField password();
 * }
 * 
 * </pre>
 * 
 * </blockquote>
 * 
 * @author Morten Hermansen
 */

public interface Screen extends View {

}
