package net.morher.ui.connect.api.handlers;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.element.ElementMetaTemplate;

public interface ElementMethodInvocation<E extends Element, L> extends ElementMethodContext<E, L> {

    Object forwardInvocation(Object toObject) throws InvocationTargetException, IllegalAccessException, IllegalArgumentException;

    Object forwardToDefaultImplementation() throws Throwable;

    <S extends Element> Optional<S> getChildElement(ElementMetaTemplate<S> metaTemplate, L childLink, Integer index);

    <S extends Element> S childElementNotFound(ElementMetaTemplate<S> metaTemplate);

}
