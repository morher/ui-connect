package net.morher.ui.connect.api.utils;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public abstract class DefaultMethodHandles {
    private static final Constructor<Lookup> constructor = createConstructor();

    private DefaultMethodHandles() {
    }

    private static Constructor<Lookup> createConstructor() {

        try {
            Constructor<Lookup> constructor = Lookup.class.getDeclaredConstructor(Class.class);
            if ((!Modifier.isPublic(constructor.getModifiers()) ||
                    !Modifier.isPublic(constructor.getDeclaringClass().getModifiers()))
                    && !constructor.isAccessible()) {
                constructor.setAccessible(true);
            }
            return constructor;

        } catch (NoSuchMethodException | SecurityException e) {
            return null;
        }
    }

    public static MethodHandle lookup(Method method) {
        try {
            // if constructor is not null, try Java 8 lookup.
            if (constructor != null) {
                return constructor
                        .newInstance(method.getDeclaringClass())
                        .unreflectSpecial(method, method.getDeclaringClass());

            }

            return MethodHandles
                    .lookup()
                    .findSpecial(
                            method.getDeclaringClass(), method.getName(),
                            MethodType.methodType(method.getReturnType(), method.getParameterTypes()),
                            method.getDeclaringClass());

        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

}
