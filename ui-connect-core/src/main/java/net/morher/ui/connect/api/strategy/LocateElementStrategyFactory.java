package net.morher.ui.connect.api.strategy;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import net.morher.ui.connect.api.ApplicationDefinition.ApplicationParsingQueue;
import net.morher.ui.connect.api.annotation.Alias;
import net.morher.ui.connect.api.annotation.Secret;
import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.element.ElementMetaTemplate;
import net.morher.ui.connect.api.handlers.ElementMethodInvocation;
import net.morher.ui.connect.api.handlers.MethodHandler;
import net.morher.ui.connect.api.mapping.ElementLocater;
import net.morher.ui.connect.api.mapping.LocatorDescription;
import net.morher.ui.connect.api.mapping.UserInterfaceMapper;

public class LocateElementStrategyFactory implements MethodStrategyFactory {

    @Override
    public MethodStrategy getMethodStrategy(Method method) {
        Class<?> returnType = method.getReturnType();

        if (Element.class.isAssignableFrom(returnType)) {
            return new LocateElementStrategy<>((Class<? extends Element>) returnType, method, ElementReturnType.SINGLE);
        }

        if (Optional.class.equals(returnType)) {
            Class<? extends Element> elementType = getGenericParameterElementType(method);
            if (elementType != null) {
                return new LocateElementStrategy<>(elementType, method, ElementReturnType.OPTIONAL);
            }
        }

        if (Iterable.class.equals(method.getReturnType())
                || Collection.class.equals(method.getReturnType())
                || List.class.equals(method.getReturnType())) {

            Class<? extends Element> elementType = getGenericParameterElementType(method);
            if (elementType != null) {
                return new LocateElementStrategy<>(elementType, method, ElementReturnType.LIST);
            }
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    private static Class<? extends Element> getGenericParameterElementType(Method method) {
        Type type = method.getGenericReturnType();
        if (type instanceof ParameterizedType) {
            ParameterizedType pType = (ParameterizedType) type;
            Type genericArgument = pType.getActualTypeArguments()[0];

            if (genericArgument instanceof Class<?>
                    && Element.class.isAssignableFrom((Class<?>) genericArgument)) {
                return (Class<? extends Element>) genericArgument;
            }
        }
        return null;
    }

    public static <S extends Element> ElementMetaTemplate<S> getElementMetaTemplate(Class<S> elementType, Method method) {

        return new ElementMetaTemplate<>(
                elementType,
                getElementName(elementType, method),
                isSecret(elementType, method));
    }

    public static String getElementName(Class<? extends Element> elementType, Method method) {
        Alias methodAlias = method.getAnnotation(Alias.class);
        if (methodAlias != null && methodAlias.value().length >= 1) {
            return methodAlias.value()[0];
        }

        Alias elementAlias = elementType.getAnnotation(Alias.class);
        if (elementAlias != null && elementAlias.value().length >= 1) {
            return elementAlias.value()[0];
        }

        return method.getName();
    }

    public static boolean isSecret(Class<? extends Element> elementType, Method method) {
        return elementType.getAnnotation(Secret.class) != null
                || method.getAnnotation(Secret.class) != null;
    }

    private static class LocateElementStrategy<S extends Element> extends MethodStrategy implements LocatorDescription {
        private final Class<S> elementType;
        private final Method method;
        private final ElementReturnType returnType;
        private final ElementMetaTemplate<S> metaTemplate;

        public LocateElementStrategy(Class<S> elementType, Method method, ElementReturnType returnType) {
            this.elementType = elementType;
            this.method = method;
            this.returnType = returnType;
            this.metaTemplate = getElementMetaTemplate(elementType, method);
        }

        @Override
        public void contributeToParsingQueue(ApplicationParsingQueue ctx) {
            ctx.addElementType(elementType);
        }

        @Override
        public <L> MethodHandler<L> buildHandler(UserInterfaceMapper<L> uiMapper, Method method) {
            ElementLocater<L> locator = uiMapper.buildLocator(this);
            if (locator == null) {

                return new UnknownMethodHandler<>(method, "Locater for element could not be determined: " + method);
            }
            return new LocateElementHandler<>(this, locator);
        }

        @Override
        public String toString() {
            return super.toString() + " (" + elementType.getSimpleName() + ")";
        }

        @Override
        public <A extends Annotation> A findAnnotation(Class<A> annotationType) {
            A annotation = method.getAnnotation(annotationType);
            if (annotation == null) {
                annotation = elementType.getAnnotation(annotationType);
            }
            return annotation;
        }

        @Override
        public Class<S> getTargetClass() {
            return elementType;
        }

        @Override
        public boolean targetIsA(Class<?> clazz) {
            return clazz.isAssignableFrom(elementType);
        }

        public ElementReturnType getReturnType() {
            return returnType;
        }

        public ElementMetaTemplate<S> getMetaTemplate() {
            return metaTemplate;
        }
    }

    private static class LocateElementHandler<S extends Element, L> implements MethodHandler<L> {
        private final LocateElementStrategy<S> strategy;
        private final ElementLocater<L> locater;

        public LocateElementHandler(LocateElementStrategy<S> strategy, ElementLocater<L> locater) {
            this.strategy = strategy;
            this.locater = locater;
        }

        @Override
        public Object handleInvocation(ElementMethodInvocation<?, L> invocation) throws Throwable {
            Collection<L> elementLinks = locater.locate(invocation.getElementContext().getElementLink());

            return strategy
                    .getReturnType()
                    .transform(invocation, elementLinks, strategy);
        }

    }

    public static enum ElementReturnType {
        SINGLE {
            @Override
            public <L, S extends Element> S transform(ElementMethodInvocation<?, L> invocation, Collection<L> elementLinks, LocateElementStrategy<S> strategy) {
                return elementLinks
                        .stream()
                        .map((l) -> invocation.getChildElement(strategy.getMetaTemplate(), l, null))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .findFirst()
                        .orElseGet(() -> invocation.childElementNotFound(strategy.getMetaTemplate()));
            }
        },

        LIST {
            @Override
            public <L, S extends Element> List<S> transform(ElementMethodInvocation<?, L> invocation, Collection<L> elementLinks, LocateElementStrategy<S> strategy) {

                ElementSequence elementSequence = new ElementSequence(1);

                return elementLinks
                        .stream()
                        .map((l) -> invocation.getChildElement(strategy.getMetaTemplate(), l, elementSequence.get()))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .collect(Collectors.toList());
            }
        },

        OPTIONAL {
            @Override
            public <L, S extends Element> Optional<S> transform(ElementMethodInvocation<?, L> invocation, Collection<L> elementLinks, LocateElementStrategy<S> strategy) {
                return elementLinks
                        .stream()
                        .map((l) -> invocation.getChildElement(strategy.getMetaTemplate(), l, null))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .findFirst();
            }
        };

        public abstract <L, S extends Element> Object transform(ElementMethodInvocation<?, L> invocation, Collection<L> elementLinks, LocateElementStrategy<S> strategy);
    }

    private static class ElementSequence implements Supplier<Integer> {
        private int current;

        public ElementSequence(int first) {
            this.current = first;
        }

        @Override
        public Integer get() {
            return current++;
        }
    }
}
