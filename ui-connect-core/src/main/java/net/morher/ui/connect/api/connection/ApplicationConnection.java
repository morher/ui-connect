package net.morher.ui.connect.api.connection;

public interface ApplicationConnection<L> extends AutoCloseable {
    L getRootElement();
}
