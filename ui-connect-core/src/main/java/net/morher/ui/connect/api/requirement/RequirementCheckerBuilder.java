package net.morher.ui.connect.api.requirement;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.morher.ui.connect.api.element.Element;

public class RequirementCheckerBuilder {
    private List<Class<? extends RequirementCheckerFactory>> factoryClasses = new ArrayList<>();

    public static <E extends Element> RequirementChecker<? super E> buildRequirementChecker(Class<E> elementType) {
        return new RequirementCheckerBuilder()
                .registerType(elementType)
                .buildCheckerFor(elementType);
    }

    public RequirementCheckerBuilder registerType(Class<?> elementType) {

        registerRequirementAnnotations(elementType, new HashSet<>());
        for (Method method : elementType.getDeclaredMethods()) {
            registerRequirementAnnotations(method, new HashSet<>());
        }

        for (Class<?> interfaceType : elementType.getInterfaces()) {
            registerType(interfaceType);
        }
        return this;
    }

    private void registerRequirementAnnotations(AnnotatedElement annotatedElement, Set<Class<? extends Annotation>> ignoreAnnotations) {

        for (Annotation annotation : annotatedElement.getAnnotations()) {
            if (ignoreAnnotations.contains(annotation.annotationType())) {
                continue;

            } else if (annotation instanceof Requirement) {
                Requirement requirement = (Requirement) annotation;
                addFactory(requirement.factory());

            } else {
                ignoreAnnotations.add(annotation.annotationType());
                registerRequirementAnnotations(annotation.annotationType(), ignoreAnnotations);

            }
        }
    }

    public RequirementCheckerBuilder addFactory(Class<? extends RequirementCheckerFactory> factoryClass) {
        if (!factoryClasses.contains(factoryClass)) {
            factoryClasses.add(factoryClass);
        }
        return this;
    }

    public <E extends Element> RequirementChecker<? super E> buildCheckerFor(Class<E> elementType) {
        if (factoryClasses.isEmpty()) {
            return StaticRequirementChecker.PASS;
        }

        CompoundRequirementChecker<E> compoundChecker = new CompoundRequirementChecker<E>();
        for (Class<? extends RequirementCheckerFactory> factoryClass : factoryClasses) {
            compoundChecker.addAll(createRequirementChecker(factoryClass, elementType));
        }

        return compoundChecker;
    }

    public <E extends Element> Collection<RequirementChecker<? super E>> createRequirementChecker(Class<? extends RequirementCheckerFactory> factoryClass, Class<E> elementType) {
        return Requirements.factoryProvider
                .getFactory(factoryClass)
                .createRequirementChecker(elementType);
    }

}