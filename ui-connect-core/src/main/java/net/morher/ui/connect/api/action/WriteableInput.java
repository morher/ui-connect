package net.morher.ui.connect.api.action;

public interface WriteableInput extends ElementAction {

    void setValue(String value);

}
