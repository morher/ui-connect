package net.morher.ui.connect.api.strategy;

import java.lang.reflect.Method;
import net.morher.ui.connect.api.handlers.ElementMethodInvocation;
import net.morher.ui.connect.api.handlers.MethodHandler;
import net.morher.ui.connect.api.mapping.UserInterfaceMapper;

public class ElementPassThroughStrategyFactory implements MethodStrategyFactory {
    private static final PassThroughStrategy STRATEGY = new PassThroughStrategy();
    private final Class<?>[] interfaceClasses;

    public ElementPassThroughStrategyFactory(Class<?>... interfaceClasses) {
        this.interfaceClasses = interfaceClasses;
    }

    @Override
    public MethodStrategy getMethodStrategy(Method method) {
        for (Class<?> interfaceClass : interfaceClasses) {
            if (interfaceClass.equals(method.getDeclaringClass())) {
                return STRATEGY;
            }
        }

        return null;
    }

    private static class PassThroughStrategy extends MethodStrategy {

        @Override
        public <L> MethodHandler<L> buildHandler(UserInterfaceMapper<L> uiMapper, Method method) {
            return new PassThroughMethodHandler<>(method);
        }

    }

    private static class PassThroughMethodHandler<L> implements MethodHandler<L> {
        private final Method method;

        public PassThroughMethodHandler(Method method) {
            this.method = method;
        }

        @Override
        public Object handleInvocation(ElementMethodInvocation<?, L> invocation) throws Throwable {
            L elementLink = invocation.getElementContext().getElementLink();
            if (method.getDeclaringClass().isInstance(elementLink)) {
                invocation.forwardInvocation(elementLink);
            }
            return null;
        }

    }
}
