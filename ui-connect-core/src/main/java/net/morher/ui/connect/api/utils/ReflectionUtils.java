package net.morher.ui.connect.api.utils;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

public class ReflectionUtils {

    public static void main(String[] args) {

        Method method = Test.class.getMethods()[0];

        Type grt = method.getGenericReturnType();

        System.out.println(List.class.getTypeParameters()[0]);
        System.out.println(Collection.class.getTypeParameters()[0].getClass());

    }

    public interface Test {

        Liste liste();

    }

    public interface Liste extends List<String> {

    }
}
