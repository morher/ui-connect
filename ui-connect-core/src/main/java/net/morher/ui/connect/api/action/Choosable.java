package net.morher.ui.connect.api.action;

import java.util.List;

public interface Choosable extends ElementAction {

    void select(String option);

    List<String> getOptions();
}
