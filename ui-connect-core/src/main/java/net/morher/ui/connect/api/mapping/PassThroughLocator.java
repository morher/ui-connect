package net.morher.ui.connect.api.mapping;

import java.util.Collection;
import java.util.Collections;

public class PassThroughLocator<E> implements ElementLocater<E> {

    @Override
    public Collection<E> locate(E parentElement) {
        return Collections.singletonList(parentElement);
    }
}
