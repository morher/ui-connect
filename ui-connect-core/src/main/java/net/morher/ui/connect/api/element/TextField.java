package net.morher.ui.connect.api.element;

import net.morher.ui.connect.api.action.ElementAction;
import net.morher.ui.connect.api.action.ReadableInput;
import net.morher.ui.connect.api.action.WriteableInput;

/**
 * <p>
 * An {@link Element} Stereotype representing a text field shown in the user interface. It extends the {@link ElementAction}
 * actions {@link ReadableInput} and {@link WriteableInput}.
 * 
 * <p>
 * <strong>Example:</strong> <blockquote>
 * 
 * <pre>
 * public interface LoginScreen extends Screen {
 *     TextField username();
 * 
 *     TextField password();
 * }
 * 
 * </pre>
 * 
 * </blockquote>
 * 
 * @author Morten Hermansen
 */
public interface TextField extends Element, ReadableInput, WriteableInput {

}
