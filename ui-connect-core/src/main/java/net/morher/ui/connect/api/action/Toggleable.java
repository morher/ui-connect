package net.morher.ui.connect.api.action;

public interface Toggleable extends ElementAction {

    boolean isEnabled();

    void toggle(boolean enabled);

    void toggle();

}
