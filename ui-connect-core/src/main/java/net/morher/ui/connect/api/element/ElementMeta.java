package net.morher.ui.connect.api.element;

public interface ElementMeta {
    Class<? extends Element> getElementType();

    String getElementName();

    boolean isElementMarkedSecret();

    Element getParent();

    Integer getIndex();
}
