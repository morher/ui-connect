package net.morher.ui.connect.api.mapping;

public interface ActionHandlerFactory<E, H> {
    H buildActionHandler(E element);
}
