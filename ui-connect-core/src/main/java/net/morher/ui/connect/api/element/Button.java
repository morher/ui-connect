package net.morher.ui.connect.api.element;

import net.morher.ui.connect.api.action.Clickable;

/**
 * Represents a stereotypical Button-{@link Element} in the user interface abstraction.
 * 
 * @author Morten Hermansen
 * 
 * @see Element
 * @see Clickable
 */
public interface Button extends Element, Clickable {

}
