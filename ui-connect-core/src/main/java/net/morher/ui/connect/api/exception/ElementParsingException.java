package net.morher.ui.connect.api.exception;

public class ElementParsingException extends IllegalArgumentException {
    private static final long serialVersionUID = 1L;

    public ElementParsingException(String s) {
        super(s);
    }
}
