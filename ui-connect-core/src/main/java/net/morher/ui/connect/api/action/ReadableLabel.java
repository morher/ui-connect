package net.morher.ui.connect.api.action;

public interface ReadableLabel extends ElementAction {
    String getText();
}
