package net.morher.ui.connect.api.handlers;

import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.element.ElementMeta;

public interface ElementContext<E extends Element, L> {
    Class<E> getElementType();

    L getElementLink();

    ElementMethodContext<?, L> getFromMethod();

    ElementMeta getMeta();
}
