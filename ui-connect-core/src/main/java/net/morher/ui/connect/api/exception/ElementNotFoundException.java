package net.morher.ui.connect.api.exception;

import net.morher.ui.connect.api.element.ElementMeta;

public class ElementNotFoundException extends IllegalStateException {
    private static final long serialVersionUID = 1L;

    private final ElementMeta element;

    public ElementNotFoundException(ElementMeta element) {
        super("Could not find " + element);
        this.element = element;
    }

    public ElementMeta getElement() {
        return element;
    }

}
