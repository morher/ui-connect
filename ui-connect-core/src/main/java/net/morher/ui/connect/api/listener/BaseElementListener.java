package net.morher.ui.connect.api.listener;

import net.morher.ui.connect.api.handlers.ElementMethodInvocation;
import net.morher.ui.connect.api.handlers.MethodHandler;

public class BaseElementListener<L> implements ElementListener<L> {

    @Override
    public void beforeInvocation(ElementMethodInvocation<?, ? extends L> invocation, MethodHandler<? extends L> handler) {
        // Do nothing
    }

    @Override
    public void afterInvocation(ElementMethodInvocation<?, ? extends L> invocation, MethodHandler<? extends L> handler, Object returnValue) {
        // Do nothing
    }

    @Override
    public void invocationException(ElementMethodInvocation<?, ? extends L> invocation, MethodHandler<? extends L> handler, Exception e) {
        // Do nothing
    }

}
