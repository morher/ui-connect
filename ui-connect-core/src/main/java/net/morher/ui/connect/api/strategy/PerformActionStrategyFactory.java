package net.morher.ui.connect.api.strategy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import net.morher.ui.connect.api.action.ElementAction;
import net.morher.ui.connect.api.handlers.ElementMethodInvocation;
import net.morher.ui.connect.api.handlers.MethodHandler;
import net.morher.ui.connect.api.mapping.ActionHandlerFactory;
import net.morher.ui.connect.api.mapping.UserInterfaceMapper;

public class PerformActionStrategyFactory implements MethodStrategyFactory {

    @Override
    public MethodStrategy getMethodStrategy(Method method) {
        return ElementAction.class.isAssignableFrom(method.getDeclaringClass())
                ? new PerformActionStrategy()
                : null;
    }

    private static class PerformActionStrategy extends MethodStrategy {

        @Override
        public <L> MethodHandler<L> buildHandler(UserInterfaceMapper<L> uiMapper, Method method) {
            ActionHandlerFactory<L, ?> actionHandlerFactory = uiMapper.getActionHandlerFactory();
            return new PerformActionHandler<>(method, actionHandlerFactory);
        }
    }

    private static class PerformActionHandler<L> implements MethodHandler<L> {
        private final Method method;
        private final ActionHandlerFactory<L, ?> actionHandlerFactory;

        public PerformActionHandler(Method method, ActionHandlerFactory<L, ?> actionHandlerFactory) {
            this.method = method;
            this.actionHandlerFactory = actionHandlerFactory;
        }

        @Override
        public Object handleInvocation(ElementMethodInvocation<?, L> invocation) throws Throwable {
            Object handler = actionHandlerFactory.buildActionHandler(invocation.getElementContext().getElementLink());
            if (method.getDeclaringClass().isInstance(handler)) {
                try {
                    return invocation.forwardInvocation(handler);
                } catch (InvocationTargetException ite) {
                    throw ite.getCause();
                }
            }
            throw new IllegalStateException("Incompatible action handler " + handler + " for " + invocation);
        }
    }
}
