package net.morher.ui.connect.api.mapping;

public class ContextPassThroughActionHandlerFactory<C> implements ActionHandlerFactory<C, C> {

    @Override
    public C buildActionHandler(C context) {
        return context;
    }

}
