package net.morher.ui.connect.api.requirement;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;

import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.exception.ElementNotFoundException;

/**
 * Annotate an element-method to indicate that the sub-element is required for the containing element to be considered found.
 * 
 * @author Morten Hermansen
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
@Requirement(factory = RequiredElement.Factory.class)
public @interface RequiredElement {

    public static class Factory implements RequirementCheckerFactory {

        @Override
        public <E extends Element> Collection<RequirementChecker<? super E>> createRequirementChecker(Class<E> elementType) {
            Collection<RequirementChecker<? super E>> checkers = new ArrayList<>();
            for (Method method : elementType.getMethods()) {
                if (method.getAnnotation(RequiredElement.class) != null) {
                    checkers.add(new Checker(method));
                }
            }
            return checkers;
        }

    }

    public static class Checker implements RequirementChecker<Element> {
        private final Method elementLocator;

        public Checker(Method elementLocator) {
            if (!Element.class.isAssignableFrom(elementLocator.getReturnType())
                    || elementLocator.getParameterCount() != 0) {
                throw new IllegalArgumentException("RequiredElement can only be put on methods with no parameters returning an Element. "
                        + "Cannot create check for " + elementLocator.getName() + " on " + elementLocator.getDeclaringClass());
            }

            this.elementLocator = elementLocator;
        }

        @Override
        public boolean check(Element element) {
            try {
                elementLocator.invoke(element);
                return true;

            } catch (InvocationTargetException e) {
                if (ElementNotFoundException.class.isInstance(e.getTargetException())) {
                    return false;
                }
                throw new IllegalStateException("Unexpected exception during requirement check", e.getTargetException());

            } catch (IllegalAccessException | IllegalArgumentException e) {
                throw new IllegalStateException("Unexpected exception during requirement check", e);
            }
        }

    }
}
