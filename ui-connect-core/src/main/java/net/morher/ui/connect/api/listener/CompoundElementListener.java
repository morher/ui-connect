package net.morher.ui.connect.api.listener;

import java.util.ArrayList;
import java.util.List;

import net.morher.ui.connect.api.handlers.ElementMethodInvocation;
import net.morher.ui.connect.api.handlers.MethodHandler;

public class CompoundElementListener<L> implements ElementListener<L> {
    private final List<ElementListener<? super L>> delegates = new ArrayList<>();

    public static <L> ElementListener<L> of(Iterable<ElementListener<? super L>> delegates) {
        CompoundElementListener<L> compoundElementListener = new CompoundElementListener<>();
        for (ElementListener<? super L> delegate : delegates) {
            compoundElementListener.addDelegate(delegate);
        }
        return compoundElementListener;
    }

    public void addDelegate(ElementListener<? super L> delegate) {
        delegates.add(delegate);
    }

    @Override
    public void beforeInvocation(ElementMethodInvocation<?, ? extends L> invocation, MethodHandler<? extends L> handler) {
        for (ElementListener<? super L> delegate : delegates) {
            delegate.beforeInvocation(invocation, handler);
        }
    }

    @Override
    public void afterInvocation(ElementMethodInvocation<?, ? extends L> invocation, MethodHandler<? extends L> handler, Object returnValue) {
        for (ElementListener<? super L> delegate : delegates) {
            delegate.afterInvocation(invocation, handler, returnValue);
        }
    }

    @Override
    public void invocationException(ElementMethodInvocation<?, ? extends L> invocation, MethodHandler<? extends L> handler, Exception e) {
        for (ElementListener<? super L> delegate : delegates) {
            delegate.invocationException(invocation, handler, e);
        }
    }

}
