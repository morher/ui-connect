package net.morher.ui.connect.api.element;

import net.morher.ui.connect.api.action.ElementAction;

/**
 * The {@code Element} is the base abstraction for the user interface. An {@code Element} can contain other fine grained
 * elements and represents everything from the {@link Application} and {@link Screen} down to the single {@link Label},
 * {@link TextField} and {@link Button}.
 * 
 * The {@code Element} should be extended by an interface describing the relevant part of the user interface. The methods could
 * be either element locators, element list locators or methods with default implementation. In addition, the extending
 * interface could also extend any number of {@link ElementAction} interfaces to define available actions for the element.
 * 
 * @author Morten Hermansen
 */
public interface Element extends ElementMeta {
    
}
