package net.morher.ui.connect.api.utils;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;

import net.morher.ui.connect.api.element.Application;
import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.element.ElementMeta;

public class ElementHierarchy implements Iterable<Element> {
    private final ElementMeta elementMeta;

    private ElementHierarchy(ElementMeta elementMeta) {
        this.elementMeta = elementMeta;
    }

    public static ElementHierarchy from(ElementMeta elementMeta) {
        return new ElementHierarchy(elementMeta);
    }

    @SuppressWarnings("unchecked")
    public <S extends Element> Optional<S> findClosestSurrounding(Class<S> elementType) {
        for (Element current : this) {
            if (elementType.isInstance(current)) {
                return Optional.of((S) current);
            }
        }
        return Optional.empty();
    }

    @SuppressWarnings("unchecked")
    public <A extends Application> A getApplication(Class<A> applicationType) {
        ElementMeta root = getRoot();

        if (!applicationType.isInstance(root)) {
            throw new IllegalStateException("Application was not of type " + applicationType.getSimpleName() + ", but " + root.getElementType());
        }

        return (A) root;
    }

    public ElementMeta getRoot() {
        ElementMeta root = elementMeta;
        for (Element element : this) {
            root = element;
        }
        return root;
    }

    @Override
    public Iterator<Element> iterator() {
        return new ParentIterator(elementMeta);
    }

    private static class ParentIterator implements Iterator<Element> {
        private Element next;

        public ParentIterator(ElementMeta element) {
            this.next = element.getParent();
        }

        @Override
        public boolean hasNext() {
            return next != null;
        }

        @Override
        public Element next() {
            if (next == null) {
                throw new NoSuchElementException();
            }
            Element current = next;
            next = current.getParent();
            return current;
        }
    }
}
