package net.morher.ui.connect.api.strategy;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import net.morher.ui.connect.api.handlers.ElementMethodInvocation;
import net.morher.ui.connect.api.handlers.MethodHandler;
import net.morher.ui.connect.api.mapping.UserInterfaceMapper;

public class CallImplementationStrategyFactory implements MethodStrategyFactory {
    private static final CallImplementationStrategy STRATEGY_INSTANCE = new CallImplementationStrategy();

    @Override
    public MethodStrategy getMethodStrategy(Method method) {
        return !Modifier.isAbstract(method.getModifiers())
                ? STRATEGY_INSTANCE
                : null;
    }

    private static class CallImplementationStrategy extends MethodStrategy {

        @Override
        public <E> MethodHandler<E> buildHandler(UserInterfaceMapper<E> uiMapper, Method method) {
            return new CallImplementationHandler<>(method);
        }

    }

    private static class CallImplementationHandler<E> implements MethodHandler<E> {
        private final Method method;

        public CallImplementationHandler(Method method) {
            this.method = method;
        }

        @Override
        public Object handleInvocation(ElementMethodInvocation<?, E> invocation) throws Throwable {
            try {
                return invocation.forwardToDefaultImplementation();

            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            }
        }

    }
}
