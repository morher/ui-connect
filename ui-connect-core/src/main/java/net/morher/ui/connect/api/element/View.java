package net.morher.ui.connect.api.element;

/**
 * An {@link Element} stereotype representing a set of related elements. Perhaps a common set of elements used multiple plasces
 * in the user interface.
 * 
 * @author Morten Hermansen
 */
public interface View extends Element {

}
