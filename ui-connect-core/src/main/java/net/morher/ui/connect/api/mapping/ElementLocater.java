package net.morher.ui.connect.api.mapping;

import java.util.Collection;

public interface ElementLocater<E> {
    Collection<E> locate(E parentElement);
}
