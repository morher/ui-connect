package net.morher.ui.connect.api.exception;

public class ApplicationConnectionFailedException extends IllegalArgumentException {
    private static final long serialVersionUID = 1L;

    public ApplicationConnectionFailedException(String message, Throwable e) {
        super(message, e);
    }
}
