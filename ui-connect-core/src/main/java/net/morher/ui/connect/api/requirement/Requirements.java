package net.morher.ui.connect.api.requirement;

import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.utils.FactoryProvider;

public class Requirements {

    static final FactoryProvider factoryProvider = new FactoryProvider();

    @Deprecated
    public static <E extends Element> boolean check(E element) {
        @SuppressWarnings("unchecked")
        Class<E> elementType = (Class<E>) element.getClass();

        return new RequirementCheckerBuilder()
                .registerType(elementType)
                .buildCheckerFor(elementType)
                .check(element);

    }

}
