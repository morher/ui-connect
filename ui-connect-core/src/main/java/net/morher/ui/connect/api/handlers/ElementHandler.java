package net.morher.ui.connect.api.handlers;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.element.ElementMetaTemplate;
import net.morher.ui.connect.api.requirement.RequirementChecker;
import net.morher.ui.connect.api.strategy.MethodStrategy.UnknownMethodHandler;

public class ElementHandler<E extends Element, L> {
    private final ElementMetaTemplate<E> metaTemplate;
    private final RequirementChecker<? super E> requirementChecker;
    private final Map<Method, MethodHandler<L>> methodHandlers = new HashMap<>();

    public ElementHandler(ElementMetaTemplate<E> metaTemplate, RequirementChecker<? super E> requirementChecker) {
        this.metaTemplate = metaTemplate;
        this.requirementChecker = requirementChecker;
    }

    public ElementMetaTemplate<E> getMetaTemplate() {
        return metaTemplate;
    }

    public Class<E> getElementType() {
        return metaTemplate.getElementType();
    }

    public RequirementChecker<? super E> getRequirementChecker() {
        return requirementChecker;
    }

    public void registerMethodHandler(Method method, MethodHandler<L> handler) {
        methodHandlers.put(method, handler);
    }

    public MethodHandler<L> getMethodHandler(Method method) {
        return methodHandlers.get(method);
    }

    public Collection<UnknownMethodHandler<L>> findUnhandledMethods() {
        Collection<UnknownMethodHandler<L>> unhandledMethods = new ArrayList<>();
        for (Map.Entry<Method, MethodHandler<L>> entry : methodHandlers.entrySet()) {
            MethodHandler<L> value = entry.getValue();
            if (value instanceof UnknownMethodHandler) {
                unhandledMethods.add((UnknownMethodHandler<L>) value);
            }
        }
        return unhandledMethods;
    }
}