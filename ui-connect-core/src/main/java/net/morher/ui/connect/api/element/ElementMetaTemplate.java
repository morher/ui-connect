package net.morher.ui.connect.api.element;

public class ElementMetaTemplate<E extends Element> {
    private final Class<E> elementType;
    private final String elementName;
    private final boolean elementMarkedSecret;

    public ElementMetaTemplate(Class<E> elementType, String elementName, boolean elementMarkedSecret) {
        this.elementType = elementType;
        this.elementName = elementName;
        this.elementMarkedSecret = elementMarkedSecret;
    }

    public Class<E> getElementType() {
        return elementType;
    }

    public String getElementName() {
        return elementName;
    }

    public boolean isElementMarkedSecret() {
        return elementMarkedSecret;
    }

    public ElementMetaStore<E> metaFor(Element parent, Integer index) {
        return new ElementMetaStore<>(
                elementType,
                elementName,
                elementMarkedSecret,
                parent,
                index);
    }

    @Override
    public String toString() {
        return elementName;
    }

    public static class ElementMetaStore<E extends Element> extends ElementMetaTemplate<E> implements ElementMeta {
        private final Element parent;
        private final Integer index;

        public ElementMetaStore(Class<E> elementType, String elementName, boolean elementMarkedSecret, Element parent, Integer index) {
            super(elementType, elementName, elementMarkedSecret);
            this.parent = parent;
            this.index = index;
        }

        public Element getParent() {
            return parent;
        }

        public Integer getIndex() {
            return index;
        }

        @Override
        public String toString() {
            if (index != null) {
                return new StringBuilder()
                        .append(super.toString())
                        .append(" (#")
                        .append(index)
                        .append(")")
                        .toString();
            }
            return super.toString();
        }

    }
}
