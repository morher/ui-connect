package net.morher.ui.connect.api.element;

import net.morher.ui.connect.api.action.Toggleable;

public interface Toggler extends Element, Toggleable {

}
