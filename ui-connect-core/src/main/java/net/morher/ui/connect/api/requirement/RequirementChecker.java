package net.morher.ui.connect.api.requirement;

import net.morher.ui.connect.api.element.Element;

public interface RequirementChecker<E extends Element> {
	boolean check(E element);
}
