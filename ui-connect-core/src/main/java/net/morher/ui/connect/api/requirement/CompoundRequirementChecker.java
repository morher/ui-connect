package net.morher.ui.connect.api.requirement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.morher.ui.connect.api.element.Element;

public class CompoundRequirementChecker<E extends Element> implements RequirementChecker<E> {
    private final List<RequirementChecker<? super E>> checkers = new ArrayList<>();

    public CompoundRequirementChecker<E> add(RequirementChecker<? super E> checker) {
        checkers.add(checker);
        return this;
    }

    public CompoundRequirementChecker<E> addAll(Collection<RequirementChecker<? super E>> checkers) {
        this.checkers.addAll(checkers);
        return this;
    }

    @Override
    public boolean check(E element) {
        for (RequirementChecker<? super E> checker : checkers) {
            if (!checker.check(element)) {
                return false;
            }
        }

        return true;
    }
}