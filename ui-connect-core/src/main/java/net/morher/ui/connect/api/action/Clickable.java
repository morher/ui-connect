package net.morher.ui.connect.api.action;

public interface Clickable extends ElementAction {

    void click();

    void doubleClick();
}
