package net.morher.ui.connect.api.mapping;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import net.morher.ui.connect.api.connection.ApplicationConnection;
import net.morher.ui.connect.api.connection.ApplicationConnector;
import net.morher.ui.connect.api.element.Application;
import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.handlers.ElementHandler;
import net.morher.ui.connect.api.handlers.ElementHandlerRegistry;
import net.morher.ui.connect.api.listener.CompoundElementListener;
import net.morher.ui.connect.api.listener.ElementListener;
import net.morher.ui.connect.api.strategy.ElementStrategy;
import net.morher.ui.connect.api.strategy.MethodStrategy;
import net.morher.ui.connect.api.strategy.MethodStrategy.UnknownMethodHandler;

public class ApplicationMapping<A extends Application, L> {
    private final Class<A> applicationType;
    private final ElementHandlerRegistry<L> elementHandlers;
    private final CompoundElementListener<L> listener = new CompoundElementListener<>();

    public ApplicationMapping(
            Class<A> applicationType,
            Map<Class<? extends Element>, ElementStrategy<? extends Element>> elementMappings,
            UserInterfaceMapper<L> uiMapper) {

        this.applicationType = applicationType;
        this.elementHandlers = buildMapping(elementMappings, uiMapper);
    }

    private ElementHandlerRegistry<L> buildMapping(Map<Class<? extends Element>, ElementStrategy<? extends Element>> elementMappings, UserInterfaceMapper<L> uiMapper) {
        ElementHandlerRegistry<L> elementHandlers = new ElementHandlerRegistry<>();
        for (ElementStrategy<? extends Element> elementStrategy : elementMappings.values()) {
            elementHandlers.register(buildElementMapping(elementStrategy, uiMapper));
        }
        return elementHandlers;
    }

    private <E extends Element> ElementHandler<E, L> buildElementMapping(ElementStrategy<E> elementStrategy, UserInterfaceMapper<L> uiMapper) {
        ElementHandler<E, L> elementHandler = new ElementHandler<>(elementStrategy.getMetaTemplate(), elementStrategy.getRequirementChecker());
        for (Entry<Method, MethodStrategy> entry : elementStrategy.getStrategies().entrySet()) {
            elementHandler.registerMethodHandler(entry.getKey(), entry.getValue().buildHandler(uiMapper, entry.getKey()));
        }

        return elementHandler;
    }

    public ApplicationMapping<A, L> addListener(ElementListener<? super L> listener) {
        this.listener.addDelegate(listener);
        return this;
    }

    public Collection<UnknownMethodHandler<L>> findUnhandledMethods() {
        Collection<UnknownMethodHandler<L>> unhandledMethods = new ArrayList<>();
        for (ElementHandler<?, L> elementHandler : elementHandlers.getAll()) {
            unhandledMethods.addAll(elementHandler.findUnhandledMethods());
        }
        return unhandledMethods;
    }

    public ApplicationMapping<A, L> validate() {
        Collection<UnknownMethodHandler<L>> unhandledMethods = findUnhandledMethods();
        if (!unhandledMethods.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unhandled methods found:\n");
            for (UnknownMethodHandler<L> unhandledMethod : unhandledMethods) {
                Method method = unhandledMethod.getMethod();
                sb
                        .append(" - ")
                        .append(method.getDeclaringClass().getSimpleName())
                        .append(".")
                        .append(method.getName())
                        .append(": ")
                        .append(unhandledMethod.getMessage())
                        .append("\n");
            }
            throw new IllegalStateException(sb.toString());
        }
        return this;
    }

    public A connect(ApplicationConnection<L> connection) {
        return new ApplicationConnector<L>(elementHandlers, connection, listener)
                .getRootElement(applicationType);
    }
}
