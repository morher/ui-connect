package net.morher.ui.connect.api.utils;

import java.util.HashMap;
import java.util.Map;

public class FactoryProvider {
    private final Map<Class<?>, Object> cache = new HashMap<>();

    public <F> F getFactory(Class<F> factoryClass) {

        @SuppressWarnings("unchecked")
        F factory = (F) cache.get(factoryClass);

        if (factory == null) {
            factory = createFactory(factoryClass);
            cache.put(factoryClass, factory);
        }

        return factory;
    }

    private <F> F createFactory(Class<F> factoryClass) {

        try {
            return factoryClass.getDeclaredConstructor().newInstance();

        } catch (Exception e) {
            throw new IllegalArgumentException("Could not create factory " + factoryClass.getSimpleName(), e);
        }
    }

}
