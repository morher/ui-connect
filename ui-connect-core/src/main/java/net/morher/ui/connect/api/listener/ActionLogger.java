package net.morher.ui.connect.api.listener;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.morher.ui.connect.api.action.ElementAction;
import net.morher.ui.connect.api.handlers.ElementMethodInvocation;
import net.morher.ui.connect.api.handlers.MethodHandler;

public class ActionLogger extends BaseElementListener<Object> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ActionLogger.class);

    @Override
    public void beforeInvocation(ElementMethodInvocation<?, ? extends Object> invocation, MethodHandler<? extends Object> handler) {

        if (ElementAction.class.isAssignableFrom(invocation.getMethod().getDeclaringClass())) {
            StringBuilder sb = new StringBuilder();

            appendMethodCall(sb, invocation);
            sb.append(" on ");
            sb.append(invocation.getElement());

            LOGGER.info(sb.toString());
        }
    }

    private static void appendMethodCall(StringBuilder sb, ElementMethodInvocation<?, ? extends Object> invocation) {
        Method method = invocation.getMethod();
        sb.append(method.getName());
        sb.append("(");
        Object[] args = invocation.getArgs();
        if (args != null && args.length >= 1) {
            appendValue(sb, invocation, 0);

            for (int i = 1; i < args.length; i++) {
                sb.append(", ");
                appendValue(sb, invocation, i);
            }
        }
        sb.append(")");
    }

    private static void appendValue(StringBuilder sb, ElementMethodInvocation<?, ? extends Object> invocation, int i) {
        Object arg = invocation.getArgs()[i];

        if (invocation.getElement().isElementMarkedSecret()) {
            sb.append("\"******\"");

        } else if (arg instanceof CharSequence) {
            sb.append("\"");
            sb.append(arg);
            sb.append("\"");

        } else {
            sb.append(arg);

        }
    }

    @Override
    public void afterInvocation(ElementMethodInvocation<?, ? extends Object> invocation, MethodHandler<? extends Object> handler, Object returnValue) {
        // Do nothing...
    }

}
