package net.morher.ui.connect.api.strategy;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.element.ElementMetaTemplate;
import net.morher.ui.connect.api.requirement.RequirementChecker;

public class ElementStrategy<E extends Element> {
    private final ElementMetaTemplate<E> metaTemplate;
    private final RequirementChecker<? super E> requirementChecker;
    private final Map<Method, MethodStrategy> strategies = new HashMap<>();

    public ElementStrategy(ElementMetaTemplate<E> metaTemplate, RequirementChecker<? super E> requirementChecker) {
        this.metaTemplate = metaTemplate;
        this.requirementChecker = requirementChecker;
    }

    public ElementMetaTemplate<E> getMetaTemplate() {
        return metaTemplate;
    }

    public Class<E> getElementType() {
        return metaTemplate.getElementType();
    }

    public RequirementChecker<? super E> getRequirementChecker() {
        return requirementChecker;
    }

    public void registerMethod(Method method, MethodStrategy strategy) {
        strategies.put(method, strategy);
    }

    public Map<Method, MethodStrategy> getStrategies() {
        return strategies;
    }

    public Collection<UnknownMethodStrategy> findUnknownMethods() {
        Collection<UnknownMethodStrategy> unknownMethods = new ArrayList<>();
        for (Entry<Method, MethodStrategy> entry : getStrategies().entrySet()) {
            MethodStrategy value = entry.getValue();
            if (value instanceof UnknownMethodStrategy) {
                unknownMethods.add((UnknownMethodStrategy) value);
            }
        }
        return unknownMethods;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(metaTemplate.getElementName()).append("\n");
        for (Map.Entry<Method, MethodStrategy> entry : strategies.entrySet()) {
            sb.append(" - ")
                    .append(entry.getKey().getName())
                    .append(": ")
                    .append(entry.getValue())
                    .append("\n");
        }

        return sb.toString();
    }

}