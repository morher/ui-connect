package net.morher.ui.connect.api.requirement;

import net.morher.ui.connect.api.element.Element;

public enum StaticRequirementChecker implements RequirementChecker<Element> {
    FAIL(false), PASS(true);

    private final boolean passes;

    private StaticRequirementChecker(boolean passes) {
        this.passes = passes;
    }

    @Override
    public boolean check(Element element) {
        return passes;
    }

}
