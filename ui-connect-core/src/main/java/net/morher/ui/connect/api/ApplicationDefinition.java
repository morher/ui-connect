package net.morher.ui.connect.api;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import net.morher.ui.connect.api.annotation.Alias;
import net.morher.ui.connect.api.annotation.Secret;
import net.morher.ui.connect.api.element.Application;
import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.element.ElementMetaTemplate;
import net.morher.ui.connect.api.mapping.ApplicationMapping;
import net.morher.ui.connect.api.mapping.UserInterfaceMapper;
import net.morher.ui.connect.api.requirement.RequirementCheckerBuilder;
import net.morher.ui.connect.api.strategy.ElementStrategy;
import net.morher.ui.connect.api.strategy.MethodStrategy;
import net.morher.ui.connect.api.strategy.MethodStrategyFactory;
import net.morher.ui.connect.api.strategy.UnknownMethodStrategy;

public class ApplicationDefinition<T extends Application> {
    private final Class<T> applicationType;
    private final ApplicationParserConfiguration configuration;
    private final Map<Class<? extends Element>, ElementStrategy<?>> elementMappings = new HashMap<>();

    public static <T extends Application> ApplicationDefinition<T> of(Class<T> applicationClass) {
        return new ApplicationDefinition<>(ApplicationParserConfiguration.getDefault(), applicationClass);
    }

    private ApplicationDefinition(ApplicationParserConfiguration configuration, Class<T> applicationType) {
        this.configuration = configuration;
        this.applicationType = applicationType;
        ApplicationParsingQueue ctx = new ApplicationParsingQueue();

        ctx.addElementType(applicationType);

        for (Class<? extends Element> elementType : ctx) {
            elementMappings.put(elementType, buildElementStrategy(elementType, ctx));
        }
    }

    private <E extends Element> ElementStrategy<E> buildElementStrategy(Class<E> elementType, ApplicationParsingQueue ctx) {
        ElementStrategy<E> elementStrategy = new ElementStrategy<>(
                getMetaTemplateFor(elementType),
                RequirementCheckerBuilder.buildRequirementChecker(elementType));

        for (Method method : elementType.getMethods()) {

            if (!Modifier.isStatic(method.getModifiers())
                    && !method.getDeclaringClass().isAssignableFrom(Element.class)) {
                MethodStrategy strategy = getMethodStrategy(method);
                strategy.contributeToParsingQueue(ctx);
                elementStrategy.registerMethod(method, strategy);
            }
        }

        return elementStrategy;
    }

    private static <E extends Element> ElementMetaTemplate<E> getMetaTemplateFor(Class<E> elementType) {
        String elementName = elementType.getSimpleName();
        Alias alias = elementType.getAnnotation(Alias.class);
        if (alias != null && alias.value().length > 0) {
            elementName = alias.value()[0];
        }

        boolean markedSecret = elementType.getAnnotation(Secret.class) != null;

        return new ElementMetaTemplate<>(elementType, elementName, markedSecret);
    }

    private MethodStrategy getMethodStrategy(Method method) {
        for (MethodStrategyFactory strategyFactory : configuration.getMethodStrategyFactories()) {
            MethodStrategy strategy = strategyFactory.getMethodStrategy(method);
            if (strategy != null) {
                return strategy;
            }
        }
        return new UnknownMethodStrategy(method);
    }

    public Collection<UnknownMethodStrategy> findUnknownMethods() {
        Collection<UnknownMethodStrategy> unknownMethods = new ArrayList<>();
        for (ElementStrategy elementStrategy : elementMappings.values()) {
            unknownMethods.addAll(elementStrategy.findUnknownMethods());
        }
        return unknownMethods;
    }

    public ApplicationDefinition<T> validate() {
        Collection<UnknownMethodStrategy> unknownMethods = findUnknownMethods();
        if (!unknownMethods.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unknown method strategies found:\n");
            for (UnknownMethodStrategy unknownMethod : unknownMethods) {
                Method method = unknownMethod.getMethod();
                sb
                        .append(" - ")
                        .append(method.getDeclaringClass().getSimpleName())
                        .append(".")
                        .append(method.getName())
                        .append("\n");
            }
            throw new IllegalStateException(sb.toString());
        }
        return this;
    }

    public <E> ApplicationMapping<T, E> mapWith(UserInterfaceMapper<E> uiMapper) {
        return new ApplicationMapping<>(applicationType, elementMappings, uiMapper);
    }

    public static class ApplicationParsingQueue implements Iterable<Class<? extends Element>>, Iterator<Class<? extends Element>> {
        private Set<Class<? extends Element>> classesParsed = new HashSet<>();
        private Queue<Class<? extends Element>> classesToBeParsed = new LinkedList<>();

        public void addElementType(Class<? extends Element> elementType) {
            if (!classesParsed.contains(elementType)
                    && !classesToBeParsed.contains(elementType)) {
                classesToBeParsed.add(elementType);
            }
        }

        @Override
        public boolean hasNext() {
            return !classesToBeParsed.isEmpty();
        }

        @Override
        public Class<? extends Element> next() {
            Class<? extends Element> elementType = classesToBeParsed.poll();
            classesParsed.add(elementType);
            return elementType;
        }

        @Override
        public Iterator<Class<? extends Element>> iterator() {
            return this;
        }
    }
}
