package net.morher.ui.connect.api.strategy;

import java.lang.reflect.Method;
import net.morher.ui.connect.api.ApplicationDefinition.ApplicationParsingQueue;
import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.handlers.ElementMethodInvocation;
import net.morher.ui.connect.api.handlers.MethodHandler;
import net.morher.ui.connect.api.mapping.UserInterfaceMapper;

/**
 * A MethodStrategy describes how an {@link Element}-method should be handled. Together with the {@link UserInterfaceMapper} it
 * is responsible for creating a {@link MethodHandler}.
 * 
 * @author Morten Hermansen
 */
public abstract class MethodStrategy {

    /**
     * Builds a handler based on the {@link UserInterfaceMapper}.
     * 
     * @param <E>
     *            The element type used by the {@link UserInterfaceMapper}.
     * @param uiMapper
     *            The {@link UserInterfaceMapper} used to map the methods to UI elements or actions
     * @param method
     *            The method to be mapped
     * @return A {@link MethodHandler} to handle method invocations.
     */
    public <E> MethodHandler<E> buildHandler(UserInterfaceMapper<E> uiMapper, Method method) {
        return new UnknownMethodHandler<>(method, "No MethodHandler defined for " + getClass().getSimpleName());
    }

    public void contributeToParsingQueue(ApplicationParsingQueue ctx) {
        // Override to contribute...
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    public static class UnknownMethodHandler<E> implements MethodHandler<E> {
        private final Method method;
        private final String message;

        public UnknownMethodHandler(Method method, String message) {
            this.method = method;
            this.message = message;
        }

        @Override
        public Object handleInvocation(ElementMethodInvocation<?, E> invocation) throws Throwable {
            throw new IllegalStateException(message);
        }

        public Method getMethod() {
            return method;
        }

        public String getMessage() {
            return message;
        }
    }
}
