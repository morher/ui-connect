package net.morher.ui.connect.api.handlers;

import java.lang.reflect.Method;
import net.morher.ui.connect.api.element.Element;

public interface ElementMethodContext<E extends Element, L> {
    ElementContext<E, L> getElementContext();

    E getElement();

    Method getMethod();

    Object[] getArgs();
}
