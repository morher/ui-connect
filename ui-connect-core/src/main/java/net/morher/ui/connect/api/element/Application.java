package net.morher.ui.connect.api.element;

/**
 * The {code Application} is the top level of the user interface abstraction. The application can contain multiple
 * {@link Screen}s, where usually only one is visible at the time.
 * 
 * @author Morten Hermansen
 */
public interface Application extends AutoCloseable, Element {

}
