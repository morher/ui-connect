package net.morher.ui.connect.api.strategy;

import java.lang.reflect.Method;

public final class UnknownMethodStrategy extends MethodStrategy {
    private final Method method;

    public UnknownMethodStrategy(Method method) {
        this.method = method;
    }

    public Method getMethod() {
        return method;
    }
}
