package net.morher.ui.connect.api.connection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Optional;

import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.element.ElementMeta;
import net.morher.ui.connect.api.element.ElementMetaTemplate;
import net.morher.ui.connect.api.element.ElementMetaTemplate.ElementMetaStore;
import net.morher.ui.connect.api.exception.ElementNotFoundException;
import net.morher.ui.connect.api.handlers.ElementContext;
import net.morher.ui.connect.api.handlers.ElementHandler;
import net.morher.ui.connect.api.handlers.ElementHandlerRegistry;
import net.morher.ui.connect.api.handlers.ElementMethodContext;
import net.morher.ui.connect.api.handlers.ElementMethodInvocation;
import net.morher.ui.connect.api.handlers.MethodHandler;
import net.morher.ui.connect.api.listener.ElementListener;
import net.morher.ui.connect.api.requirement.RequirementChecker;
import net.morher.ui.connect.api.utils.DefaultMethodHandles;

public class ApplicationConnector<L> {

    private final ElementHandlerRegistry<L> elementHandlers;
    private final ApplicationConnection<L> connection;
    private final ElementListener<? super L> elementListener;

    public ApplicationConnector(
            ElementHandlerRegistry<L> elementHandlers,
            ApplicationConnection<L> connection,
            ElementListener<? super L> elementListener) {

        this.elementHandlers = elementHandlers;
        this.connection = connection;
        this.elementListener = elementListener;
    }

    public <E extends Element> E getRootElement(Class<E> elementType) {
        ElementHandler<E, L> elementHandler = elementHandlers.get(elementType);

        if (elementHandler == null) {
            throw new IllegalArgumentException("No registered handler for element " + elementType.getSimpleName());
        }

        return createElement(elementHandler.getMetaTemplate(), connection.getRootElement(), null, null).get();
    }

    private <S extends Element> Optional<S> createElement(ElementMetaTemplate<S> template, L childLink, ElementMethodContext<?, L> fromMethod, Integer index) {
        // TODO: Implement...
        ElementMetaStore<S> elementMeta = template.metaFor(fromMethod != null ? fromMethod.getElement() : null, index);
        S child = new ElementMethodInvocationHandler<S>(elementMeta, childLink, fromMethod)
                .getProxy();

        RequirementChecker<? super S> checker = elementHandlers
                .get(template.getElementType())
                .getRequirementChecker();

        if (!checker.check(child)) {
            return Optional.empty();
        }

        return Optional.of(child);
    }

    private <E extends Element> Object handleInvocation(ElementMethodInvocationContext<E> invocation) throws Throwable {

        if (invocation.isMethodDeclaredWithin(ElementMeta.class)
                || invocation.isMethodDeclaredWithin(Object.class)) {
            return invocation.forwardInvocation(invocation.getElementContext().getMeta());
        }

        if (invocation.isMethodDeclaredWithin(ElementContext.class)) {
            return invocation.forwardInvocation(invocation.getElementContext());
        }

        MethodHandler<L> methodHandler = findMethodHandler(invocation);
        if (methodHandler != null) {
            try {
                beforeInvocation(invocation, methodHandler);
                Object returnValue = methodHandler.handleInvocation(invocation);
                afterInvocation(invocation, methodHandler, returnValue);
                return returnValue;

            } catch (Exception e) {
                invocationException(invocation, methodHandler, e);

                throw e;
            }

        }
        throw new IllegalStateException("No handler for method " + invocation);
    }

    private <E extends Element> MethodHandler<L> findMethodHandler(ElementMethodInvocationContext<E> invocation) {
        Class<E> element = invocation.getElementContext().getElementType();
        ElementHandler<E, L> elementHandler = elementHandlers.get(element);
        return elementHandler.getMethodHandler(invocation.getMethod());
    }

    private void beforeInvocation(ElementMethodInvocation<?, ? extends L> invocation, MethodHandler<? extends L> handler) {
        if (elementListener != null) {
            elementListener.beforeInvocation(invocation, handler);
        }
    }

    private void afterInvocation(ElementMethodInvocation<?, ? extends L> invocation, MethodHandler<? extends L> handler, Object returnValue) {
        if (elementListener != null) {
            elementListener.afterInvocation(invocation, handler, returnValue);
        }
    }

    private void invocationException(ElementMethodInvocation<?, ? extends L> invocation, MethodHandler<? extends L> handler, Exception e) {
        if (elementListener != null) {
            elementListener.invocationException(invocation, handler, e);
        }
    }

    private class ElementMethodInvocationHandler<E extends Element> implements InvocationHandler, ElementContext<E, L> {
        private final ElementMetaStore<E> meta;
        private final L elementLink;
        private final ElementMethodContext<?, L> fromMethod;
        private final E element;

        public ElementMethodInvocationHandler(ElementMetaStore<E> metaStore, L elementLink, ElementMethodContext<?, L> fromMethod) {
            this.meta = metaStore;
            this.elementLink = elementLink;
            this.fromMethod = fromMethod;
            this.element = createProxy();
        }

        @SuppressWarnings("unchecked")
        private E createProxy() {
            Class<?>[] interfaces = { meta.getElementType(), ElementContext.class };

            return (E) Proxy.newProxyInstance(
                    getClass().getClassLoader(),
                    interfaces,
                    this);
        }

        public E getProxy() {
            return element;
        }

        @Override
        public Class<E> getElementType() {
            return meta.getElementType();
        }

        @Override
        public ElementMethodContext<?, L> getFromMethod() {
            return fromMethod;
        }

        @Override
        public L getElementLink() {
            return elementLink;
        }

        @Override
        @SuppressWarnings("unchecked")
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            return handleInvocation(new ElementMethodInvocationContext<>(this, (E) proxy, method, args));
        }

        @Override
        public String toString() {
            return meta.toString();
        }

        @Override
        public ElementMetaStore<E> getMeta() {
            return meta;
        }
    }

    private class ElementMethodInvocationContext<E extends Element> implements ElementMethodInvocation<E, L> {
        private final ElementContext<E, L> elementContext;
        private final E element;
        private final Method method;
        private final Object[] args;

        public ElementMethodInvocationContext(ElementContext<E, L> elementContext, E element, Method method, Object[] args) {
            this.elementContext = elementContext;
            this.element = element;
            this.method = method;
            this.args = args;
        }

        @Override
        public ElementContext<E, L> getElementContext() {
            return elementContext;
        }

        public E getElement() {
            return element;
        }

        @Override
        public Method getMethod() {
            return method;
        }

        public boolean isMethodDeclaredWithin(Class<?> targetClass) {
            return method.getDeclaringClass().isAssignableFrom(targetClass);
        }

        public Object[] getArgs() {
            return args;
        }

        @Override
        public Object forwardInvocation(Object toObject) throws InvocationTargetException, IllegalAccessException, IllegalArgumentException {
            return method.invoke(toObject, args);
        }

        @Override
        public Object forwardToDefaultImplementation() throws InstantiationException, IllegalArgumentException, Throwable {
            try {
                return DefaultMethodHandles
                        .lookup(method)
                        .bindTo(element)
                        .invokeWithArguments(args);

            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            }
        }

        @Override
        public <S extends Element> Optional<S> getChildElement(ElementMetaTemplate<S> metaTemplate, L childLink, Integer index) {
            return createElement(metaTemplate, childLink, this, index);
        }

        @Override
        public String toString() {
            return elementContext.getElementType().getSimpleName() + "." + method.getName();
        }

        @Override
        public <S extends Element> S childElementNotFound(ElementMetaTemplate<S> metaTemplate) {
            throw new ElementNotFoundException(metaTemplate.metaFor(element, null));
        }
    }
}
