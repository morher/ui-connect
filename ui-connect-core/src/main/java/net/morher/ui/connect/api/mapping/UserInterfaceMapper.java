package net.morher.ui.connect.api.mapping;

public interface UserInterfaceMapper<E> {
    ElementLocater<E> buildLocator(LocatorDescription desc);

    ActionHandlerFactory<E, ?> getActionHandlerFactory();
}
