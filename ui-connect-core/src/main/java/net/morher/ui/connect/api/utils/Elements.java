package net.morher.ui.connect.api.utils;

import java.util.Optional;
import java.util.function.Supplier;

import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.exception.ElementNotFoundException;

public class Elements {

    public static <E extends Element> Optional<E> tryFind(Supplier<E> elementSupplier) {
        try {
            return Optional.ofNullable(elementSupplier.get());

        } catch (ElementNotFoundException e) {
            return Optional.empty();
        }
    }
}
