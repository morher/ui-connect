package net.morher.ui.connect.api;

import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.handlers.ElementContext;

public abstract class ApplicationUtils {

    public static ElementContext<?, ?> getElementContext(Element element) {
        if (element instanceof ElementContext) {
            return (ElementContext<?, ?>) element;
        }
        throw new IllegalArgumentException("No element found");
    }

}
