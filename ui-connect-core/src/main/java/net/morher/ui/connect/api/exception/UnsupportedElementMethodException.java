package net.morher.ui.connect.api.exception;

import java.lang.reflect.Method;

public class UnsupportedElementMethodException extends ElementParsingException {
    private final Method method;

    public UnsupportedElementMethodException(Method method) {
        super("Could not find a strategy to deal with " + method.getName() + " in " + method.getDeclaringClass().getSimpleName());
        this.method = method;
    }

    public Method getMethod() {
        return method;
    }
}
