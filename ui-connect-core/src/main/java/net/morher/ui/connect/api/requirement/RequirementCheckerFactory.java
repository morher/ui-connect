package net.morher.ui.connect.api.requirement;

import java.util.Collection;

import net.morher.ui.connect.api.element.Element;

public interface RequirementCheckerFactory {
    <E extends Element> Collection<RequirementChecker<? super E>> createRequirementChecker(Class<E> elementType);
}
