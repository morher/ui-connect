package net.morher.ui.connect.api.handlers;

public interface MethodHandler<L> {

    // Object handleInvocation(Object proxy, L element, ApplicationMapping<?, L> applicationMapping, Object[] args) throws
    // Throwable;

    Object handleInvocation(ElementMethodInvocation<?, L> invocation) throws Throwable;

}
