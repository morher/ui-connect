package net.morher.ui.connect.api.element;

public class BaseTestElement implements Element {
    private final Class<? extends Element> elementType;
    private final String elementName;
    private final boolean elementMarkedSecret;
    private final Element parent;

    public BaseTestElement(
            Class<? extends Element> elementType,
            String elementName,
            boolean elementMarkedSecret,
            Element parent) {

        this.elementType = elementType;
        this.elementName = elementName;
        this.elementMarkedSecret = elementMarkedSecret;
        this.parent = parent;
    }

    public BaseTestElement() {
        this(null, null, false, null);
    }

    @Override
    public Class<? extends Element> getElementType() {
        return elementType;
    }

    @Override
    public String getElementName() {
        return elementName;
    }

    @Override
    public boolean isElementMarkedSecret() {
        return elementMarkedSecret;
    }

    @Override
    public Element getParent() {
        return parent;
    }

    @Override
    public Integer getIndex() {
        return null;
    }

}
