package net.morher.ui.connect.api;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import java.util.Collection;
import net.morher.ui.connect.api.element.Application;
import net.morher.ui.connect.api.mapping.ActionHandlerFactory;
import net.morher.ui.connect.api.mapping.ElementLocater;
import net.morher.ui.connect.api.mapping.LocatorDescription;
import net.morher.ui.connect.api.mapping.UserInterfaceMapper;
import net.morher.ui.connect.api.strategy.MethodStrategy.UnknownMethodHandler;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class ApplicationMappingTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void checkFindUnknownMethods() {

        Collection<UnknownMethodHandler<Object>> unknownMethods = ApplicationDefinition
                .of(ApplicationWithUnknownMethod.class)
                .mapWith(new DummyUserInterfaceMapper())
                .findUnhandledMethods();

        assertThat(unknownMethods, is(not(nullValue())));
        assertThat(unknownMethods.size(), is(1));
        UnknownMethodHandler<Object> first = unknownMethods.iterator().next();
        assertThat(first, is(not(nullValue())));
        assertThat(first.getMethod().getName(), is(equalTo("unknownMethod")));

    }

    @Test()
    public void checkValidateThrowsException() {

        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage(containsString("Unhandled methods found:"));
        expectedException.expectMessage(containsString(" - ApplicationWithUnknownMethod.unknownMethod: No MethodHandler defined for UnknownMethodStrategy"));

        ApplicationDefinition
                .of(ApplicationWithUnknownMethod.class)
                .mapWith(new DummyUserInterfaceMapper())
                .validate();

    }

    private static class DummyUserInterfaceMapper implements UserInterfaceMapper<Object> {

        @Override
        public ElementLocater<Object> buildLocator(LocatorDescription desc) {
            return null;
        }

        @Override
        public ActionHandlerFactory<Object, ?> getActionHandlerFactory() {
            return null;
        }

    }

    private interface ApplicationWithUnknownMethod extends Application {

        String unknownMethod();

    }
}
