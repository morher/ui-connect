package net.morher.ui.connect.api.strategy;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Method;

import org.junit.Test;

import net.morher.ui.connect.api.annotation.Alias;
import net.morher.ui.connect.api.annotation.Secret;
import net.morher.ui.connect.api.element.Application;
import net.morher.ui.connect.api.element.Element;

public class LocateElementStrategyFactoryTest {

    @Test
    public void testGetElementNameFromMethod() throws Exception {
        assertThat(getElementName("methodName"), is(equalTo("methodName")));
    }

    @Test
    public void testGetElementNameFromMethodAlias() throws Exception {
        assertThat(getElementName("annotatedMethod"), is(equalTo("Method alias")));
    }

    @Test
    public void testGetElementNameFromMethodWithMultipleAliases() throws Exception {
        assertThat(getElementName("methodWithMultipleAliases"), is(equalTo("Method alias 1")));
    }

    @Test
    public void testGetElementNameFromElementWithAlias() throws Exception {
        assertThat(getElementName("elementWithAlias"), is(equalTo("Element alias")));
    }

    @Test
    public void testGetElementNameFromElementWithMultipleAliases() throws Exception {
        assertThat(getElementName("elementWithmultipleAliases"), is(equalTo("Element alias 1")));
    }

    @Test
    public void testGetElementNameFromMethodOverElement() throws Exception {
        assertThat(getElementName("elementAndMethodWithAlias"), is(equalTo("Method alias goes first")));
    }

    @Test
    public void testIsSecretNoAnnotation() {
        assertThat(isSecret("methodName"), is(false));
    }

    @Test
    public void testIsSecretMethodMarked() {
        assertThat(isSecret("secretMethod"), is(true));
    }

    @Test
    public void testIsSecretElementMarked() {
        assertThat(isSecret("secretElement"), is(true));
    }

    @SuppressWarnings("unchecked")
    private static String getElementName(String methodName) {
        try {
            Method method = TestApp.class.getMethod(methodName);
            return LocateElementStrategyFactory
                    .getElementName((Class<? extends Element>) method.getReturnType(), method);

        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private static boolean isSecret(String methodName) {
        try {
            Method method = TestApp.class.getMethod(methodName);
            return LocateElementStrategyFactory
                    .isSecret((Class<? extends Element>) method.getReturnType(), method);

        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public interface TestApp extends Application {

        TestElement methodName();

        @Alias("Method alias")
        TestElement annotatedMethod();

        @Alias({ "Method alias 1", "Method alias 2" })
        TestElement methodWithMultipleAliases();

        TestElementWithAlias elementWithAlias();

        TestElementWithMultipleAliases elementWithmultipleAliases();

        @Alias("Method alias goes first")
        TestElementWithAlias elementAndMethodWithAlias();

        @Secret
        TestElement secretMethod();

        SecretTestElement secretElement();

    }

    public interface TestElement extends Element {

    }

    @Alias("Element alias")
    public interface TestElementWithAlias extends Element {

    }

    @Alias({ "Element alias 1", "Element alias 2" })
    public interface TestElementWithMultipleAliases extends Element {

    }

    @Secret
    public interface SecretTestElement extends Element {

    }
}
