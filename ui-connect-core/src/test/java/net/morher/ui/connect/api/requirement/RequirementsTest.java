package net.morher.ui.connect.api.requirement;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collection;
import java.util.Collections;

import org.junit.Test;

import net.morher.ui.connect.api.element.BaseTestElement;
import net.morher.ui.connect.api.element.Element;

public class RequirementsTest {

    @Test
    public void testRequirementPassed() {
        boolean passed = RequirementCheckerBuilder
                .buildRequirementChecker(CheckedElement.class)
                .check(new SomePassingElement(true));

        assertThat(passed, is(true));
    }

    @Test
    public void testRequirementDoesNotPass() {
        boolean passed = RequirementCheckerBuilder
                .buildRequirementChecker(CheckedElement.class)
                .check(new SomePassingElement(false));

        assertThat(passed, is(false));
    }

    @Test
    public void testNoRequirement() {
        boolean passed = RequirementCheckerBuilder
                .buildRequirementChecker(PassingElement.class)
                .check(new SomePassingElement(false));

        assertThat(passed, is(true));
    }

    @Test
    public void testRequirementInSubElementNotPassed() {
        boolean passed = RequirementCheckerBuilder
                .buildRequirementChecker(CheckedSubElement.class)
                .check(new SomePassingElement(false));

        assertThat(passed, is(false));
    }

    @Test
    public void testRequirementOnMethodNotPassed() {
        boolean passed = RequirementCheckerBuilder
                .buildRequirementChecker(CheckedSubElement.class)
                .check(new SomePassingElement(false));

        assertThat(passed, is(false));
    }

    public interface PassingElement extends Element {
        boolean isPassed();
    }

    @RequirePassed
    public interface CheckedElement extends PassingElement {
    }

    public interface MethodRequirementElement extends PassingElement {

        void someMethodThatRequiresPassed();
    }

    public static class SomePassingElement extends BaseTestElement implements PassingElement, CheckedElement, CheckedSubElement, MethodRequirementElement {
        private final boolean passed;

        public SomePassingElement(boolean passed) {
            this.passed = passed;
        }

        @Override
        public boolean isPassed() {
            return passed;
        }

        @Override
        public void someMethodThatRequiresPassed() {
            // Do nothing...
        }
    }

    public interface CheckedSubElement extends CheckedElement {

    }

    public static class RequiredPassedCheckerFactory implements RequirementCheckerFactory {

        @Override
        @SuppressWarnings("unchecked")
        public <E extends Element> Collection<RequirementChecker<? super E>> createRequirementChecker(Class<E> elementType) {
            if (CheckedElement.class.isAssignableFrom(elementType)) {
                return Collections.singletonList((RequirementChecker<E>) new RequiredPassedChecker());
            }
            throw new IllegalStateException();
        }

        public boolean check(CheckedElement element) {
            return element.isPassed();
        }

    }

    public static class RequiredPassedChecker implements RequirementChecker<CheckedElement> {

        @Override
        public boolean check(CheckedElement element) {
            return element.isPassed();
        }
    }

    @Target({ ElementType.TYPE, ElementType.METHOD })
    @Retention(RetentionPolicy.RUNTIME)
    @Requirement(factory = RequiredPassedCheckerFactory.class)
    public @interface RequirePassed {

    }
}
