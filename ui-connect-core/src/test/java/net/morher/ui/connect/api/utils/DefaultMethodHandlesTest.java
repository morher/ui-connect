package net.morher.ui.connect.api.utils;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.junit.Test;

public class DefaultMethodHandlesTest {
    Method defaultMethod = TestInterface.class.getMethods()[0];

    @Test
    public void testInvokeDefaultMethod() throws Throwable {
        assertThat(getProxy().defaultMethod(), is(equalTo("From proxy")));

        assertThat(DefaultMethodHandles
                .lookup(defaultMethod)
                .bindTo(getProxy())
                .invoke(), is(equalTo("From default method")));

    }

    public TestInterface getProxy() {
        return (TestInterface) Proxy.newProxyInstance(
                getClass().getClassLoader(),
                new Class<?>[] { TestInterface.class },
                (Object proxy, Method method, Object[] args) -> {
                    return "From proxy";
                });
    }

    public interface TestInterface {

        default String defaultMethod() {
            return "From default method";
        }
    }
}
