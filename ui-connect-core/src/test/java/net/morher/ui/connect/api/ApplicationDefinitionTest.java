package net.morher.ui.connect.api;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import java.util.Collection;
import net.morher.ui.connect.api.element.Application;
import net.morher.ui.connect.api.element.View;
import net.morher.ui.connect.api.strategy.UnknownMethodStrategy;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class ApplicationDefinitionTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void checkFindUnknownMethods() {

        Collection<UnknownMethodStrategy> unknownMethods = ApplicationDefinition
                .of(ApplicationWithUnknownMethod.class)
                .findUnknownMethods();

        assertThat(unknownMethods, is(not(nullValue())));
        assertThat(unknownMethods.size(), is(1));
        UnknownMethodStrategy first = unknownMethods.iterator().next();
        assertThat(first, is(not(nullValue())));
        assertThat(first.getMethod().getName(), is(equalTo("unknownMethod")));

    }

    @Test()
    public void checkValidateThrowsException() {

        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage(containsString("Unknown method strategies found:"));
        expectedException.expectMessage(containsString(" - ApplicationWithUnknownMethod.unknownMethod"));

        ApplicationDefinition
                .of(ApplicationWithUnknownMethod.class)
                .validate();

    }

    private interface ApplicationWithUnknownMethod extends Application {

        View view();

        String unknownMethod();

    }
}
