package net.morher.ui.connect.api.strategy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

import net.morher.ui.connect.api.element.Element;
import net.morher.ui.connect.api.element.ElementMetaTemplate;
import net.morher.ui.connect.api.handlers.ElementContext;
import net.morher.ui.connect.api.handlers.ElementMethodInvocation;
import net.morher.ui.connect.api.handlers.MethodHandler;
import net.morher.ui.connect.api.utils.DefaultMethodHandles;

public class AbstractStrategyTest {

    public static Method findMethod(Class<?> cls, String methodName) {
        return null;
    }

    public static class PassThroughInvocationHandler<E extends Element, L> implements InvocationHandler {
        private final MethodHandler<L> handler;
        private final E element;

        public PassThroughInvocationHandler(MethodHandler<L> handler, E element) {
            this.handler = handler;
            this.element = element;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

            if (Object.class.equals(method.getDeclaringClass())) {
                return method.invoke(this, args);
            }

            ElementMethodInvocation<E, L> invocation = new TestMethodInvocation<E, L>(null, (E) proxy, method, args);

            return handler.handleInvocation(invocation);
        }
    }

    public static class TestMethodInvocation<E extends Element, L> implements ElementMethodInvocation<E, L> {
        private final ElementContext<E, L> elementContext;
        private final E element;
        private final Method method;
        private final Object[] args;

        public TestMethodInvocation(ElementContext<E, L> elementContext, E proxy, Method method, Object[] args) {
            this.elementContext = elementContext;
            this.element = proxy;
            this.method = method;
            this.args = args;
        }

        @Override
        public ElementContext<E, L> getElementContext() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public E getElement() {
            return element;
        }

        @Override
        public Method getMethod() {
            return method;
        }

        @Override
        public Object[] getArgs() {
            return args;
        }

        @Override
        public Object forwardInvocation(Object toObject) throws InvocationTargetException, IllegalAccessException, IllegalArgumentException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Object forwardToDefaultImplementation() throws Throwable {
            try {
                return DefaultMethodHandles
                        .lookup(method)
                        .bindTo(element)
                        .invokeWithArguments(args);

            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            }
        }

        @Override
        public <S extends Element> Optional<S> getChildElement(ElementMetaTemplate<S> metaTemplate, L childLink, Integer index) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public <S extends Element> S childElementNotFound(ElementMetaTemplate<S> metaTemplate) {
            // TODO Auto-generated method stub
            return null;
        }

    }

}
