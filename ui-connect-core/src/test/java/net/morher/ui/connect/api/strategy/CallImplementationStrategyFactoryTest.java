package net.morher.ui.connect.api.strategy;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeThat;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import net.morher.ui.connect.api.element.Application;
import net.morher.ui.connect.api.handlers.MethodHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class CallImplementationStrategyFactoryTest extends AbstractStrategyTest {
    public static final String RETURN_VALUE = "Value returned from default method";

    @Test
    public void noMethodStrategyForUnimplementedMethod() throws NoSuchMethodException, SecurityException {

        MethodStrategy methodStrategy = new CallImplementationStrategyFactory()
                .getMethodStrategy(TestApplication.class.getMethod("unimplemented", new Class<?>[0]));

        assertThat(methodStrategy, is(nullValue()));
    }

    @Test
    public void methodStrategyForDefaultImplementationMethod() throws NoSuchMethodException, SecurityException {

        MethodStrategy methodStrategy = new CallImplementationStrategyFactory()
                .getMethodStrategy(defaultImplementationMethod());

        assertThat(methodStrategy, is(not(nullValue())));
    }

    @Test
    public void methodStrategyCreatesHandler() throws NoSuchMethodException, SecurityException {

        Method method = defaultImplementationMethod();
        MethodStrategy methodStrategy = new CallImplementationStrategyFactory()
                .getMethodStrategy(method);

        assumeThat(methodStrategy, is(not(nullValue())));

        MethodHandler<Object> handler = methodStrategy.buildHandler(null, method);

        assertThat(handler, is(not(nullValue())));
    }

    @Test
    public void methodStrategyCallsDefaultImplementation() throws Throwable {

        Method method = defaultImplementationMethod();
        MethodStrategy methodStrategy = new CallImplementationStrategyFactory()
                .getMethodStrategy(method);

        assumeThat(methodStrategy, is(not(nullValue())));

        MethodHandler<Object> handler = methodStrategy.buildHandler(null, method);

        assumeThat(handler, is(not(nullValue())));

        TestApplication proxy = (TestApplication) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class<?>[] { TestApplication.class },
                new PassThroughInvocationHandler<TestApplication, Object>(handler, null));

        String result = proxy.doSomething();

        assertThat(result, is(equalTo(RETURN_VALUE)));
    }

    private static Method defaultImplementationMethod() throws NoSuchMethodException {
        return TestApplication.class.getMethod("doSomething", new Class<?>[0]);
    }

    public static interface TestApplication extends Application {

        String unimplemented();

        default String doSomething() {
            return RETURN_VALUE;
        }
    }
}
